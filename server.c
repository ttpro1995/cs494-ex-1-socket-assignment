
//Thai Thien 1351040

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
     if (newsockfd < 0) 
          error("ERROR on accept");
     bzero(buffer,256);
     n = read(newsockfd,buffer,255);
     if (n < 0) error("ERROR reading from socket");

    //buffer
   // buffer is receiver from client
    // calculate sum, mul of all number
    // calculate sub, div of first and second number
     char* pch = strtok(buffer," ");
     int sum =0;
     int tich = 1;
     int aa = 0; // get first number
     int bb = 0; // get second number

     while(pch!=NULL){
        int a = atoi(pch);
        if (aa ==0)
            aa = a;
        else if ((aa!=0) && (bb ==0))
            bb = a;
        sum = sum + a;
        tich = tich * a;
        pch = strtok(NULL," ");
     }

     int sub = aa-bb;
     float division = (float) aa  / (float) bb;     
     printf("Sum of all number is %d \n",sum);
     printf("Multiply of all number is %d \n", tich);
     printf("a %d \n",aa);
     printf("b %d \n",bb);
     printf("a - b = %d \n", sub);
     printf("a / b = %f \n",division);
     
     char str0[255];
     char str1[255];
     char str2[255];
     char str3[255];
     char str4[255];
     sprintf(str0, "Answer from server at port %d \n", portno);
     sprintf(str1, "Sum of all number is %d \n", sum);
     sprintf(str2, "Multiply of all number is %d \n", tich);
     sprintf(str3, "a - b = %d \n", sub);
     sprintf(str4, "a / b = %f \n",division);
     char str[255];
     strcpy(str, str0);
     strcat(str, str1);
     strcat(str,str2);
     strcat(str,str3);
     strcat(str,str4);
     strcat(str,"That 's all !!!");
     // n = write(newsockfd,"I got your message",18);
     n = write(newsockfd,str,255);
     if (n < 0) error("ERROR writing to socket");
     close(newsockfd);
     close(sockfd);
     return 0; 
}
